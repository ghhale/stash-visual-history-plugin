package com.terrafolio.stash.history;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.stash.content.Changeset;
import com.atlassian.stash.history.HistoryService;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.repository.RepositoryService;
import com.atlassian.stash.util.Page;
import com.atlassian.stash.util.PageRequestImpl;
import com.google.common.collect.ImmutableMap;

@SuppressWarnings("serial")
public class VisualHistoryServlet extends HttpServlet {
	private final SoyTemplateRenderer soyTemplateRenderer;
	private final RepositoryService repositoryService;
	private final HistoryService historyService;
	private final WebResourceManager webResourceManager;

	public VisualHistoryServlet(SoyTemplateRenderer soyTemplateRenderer,
			RepositoryService repositoryService, HistoryService historyService,
			WebResourceManager webResourceManager) {
		super();
		this.soyTemplateRenderer = soyTemplateRenderer;
		this.repositoryService = repositoryService;
		this.historyService = historyService;
		this.webResourceManager = webResourceManager;
	}

	protected void render(HttpServletResponse resp, String templateName, Map<String, Object> data) throws IOException, ServletException {
        resp.setContentType("text/html;charset=UTF-8");
        try {
        	webResourceManager.requireResourcesForContext("com.terrafolio.stash-visual-history-plugin");
            soyTemplateRenderer.render(resp.getWriter(),
                    "com.terrafolio.stash-visual-history-plugin:history-soy",
                    templateName,
                    data);
        } catch (SoyException e) {
            Throwable cause = e.getCause();
            if (cause instanceof IOException) {
                throw (IOException) cause;
            }
            throw new ServletException(e);
        }
    }
	
	@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // Get repoSlug from path
        String pathInfo = req.getPathInfo();

        String[] components = pathInfo.split("/");

        if (components.length < 3) {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }

        Repository repository = repositoryService.getBySlug(components[1], components[2]);
        
        if (repository == null) {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }

        render(resp, "com.terrafolio.stash.history.visualization", ImmutableMap.<String, Object>of("repository", repository));
    }
}
