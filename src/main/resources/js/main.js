//
//  main.js
//
//  A project template for using arbor.js
//

//(function($) {

    var heads = {}
    var project
    var selectedBranches = [ 'master' ]
    var highlighted
    var slug
	var sys
	var _$ = jQuery
	var limit = 20
	
	// Given two points, shorten the line between them by w
	var shorten = function(pt1, pt2, w) {
		var x = Math.abs(pt1.x-pt2.x)
		var y = Math.abs(pt1.y-pt2.y)
		var z = Math.sqrt(Math.pow(x,2) + Math.pow(y,2))
		
		if (pt1.x > pt2.x) { 
			//pt1.x -= w
			pt2.x += x*w/z
		} else {
			//pt1.x += w
			pt2.x -= x*w/z
		}
		if (pt1.y > pt2.y) { 
			//pt1.y -= w
			pt2.y += y*w/z
		} else {
			//pt1.y += w
			pt2.y -= y*w/z
		}
		
		return [ pt1, pt2 ]
	}
	
	var Renderer = function(canvas) {
		var canvas = _$(canvas).get(0)
		var ctx = canvas.getContext("2d");
		var particleSystem

		var that = {
			init : function(system) {
				//
				// the particle system will call the init function once, right
				// before the
				// first frame is to be drawn. it's a good place to set up the
				// canvas and
				// to pass the canvas size to the particle system
				//
				// save a reference to the particle system for use in the
				// .redraw() loop
				particleSystem = system

				// inform the system of the screen dimensions so it can map
				// coords for us.
				// if the canvas is ever resized, screenSize should be called
				// again with
				// the new dimensions
				particleSystem.screenSize(canvas.width, canvas.height)
				particleSystem.screenPadding(40) // leave an extra 80px of
													// whitespace per side

				// set up some event handlers to allow for node-dragging
				that.initMouseHandling()
				
				that.initDoubleClickHandling()
			},

			redraw : function() {
				// 
				// redraw will be called repeatedly during the run whenever the
				// node positions
				// change. the new positions for the nodes can be accessed by
				// looking at the
				// .p attribute of a given node. however the p.x & p.y values
				// are in the coordinates
				// of the particle system rather than the screen. you can either
				// map them to
				// the screen yourself, or use the convenience iterators
				// .eachNode (and .eachEdge)
				// which allow you to step through the actual node objects but
				// also pass an
				// x,y point in the screen's coordinate system
				// 
				ctx.fillStyle = "white"
				ctx.fillRect(0, 0, canvas.width, canvas.height)
				
				var w = 4

				particleSystem.eachEdge(function(edge, _pt1, _pt2) {
					// edge: {source:Node, target:Node, length:#, data:{}}
					// pt1: {x:#, y:#} source position in screen coords
					// pt2: {x:#, y:#} target position in screen coords
					var pts = shorten(_pt1, _pt2, w)
					var pt1 = pts[0]
					var pt2 = pts[1]

					// draw a line from pt1 to pt2
					ctx.strokeStyle = "rgba(0,0,0, .333)"
					ctx.lineWidth = 1
					ctx.beginPath()
					ctx.moveTo(pt1.x, pt1.y)
					ctx.lineTo(pt2.x, pt2.y)
					ctx.stroke()

					ctx.save()
					// move to the head position of the edge we just drew
					var wt = 2
					var arrowLength = 6 + wt
					var arrowWidth = 2 + wt
					ctx.fillStyle = "#cccccc"
					ctx.translate(pt2.x, pt2.y);
					ctx.rotate(Math.atan2(pt2.y - pt1.y, pt2.x - pt1.x));

					// delete some of the edge that's already there (so the
					// point isn't hidden)
					ctx.clearRect(-arrowLength / 2, -wt / 2, arrowLength / 2, wt)

					// draw the chevron
					ctx.beginPath();
					ctx.moveTo(-arrowLength, arrowWidth);
					ctx.lineTo(0, 0);
					ctx.lineTo(-arrowLength, -arrowWidth);
					ctx.lineTo(-arrowLength * 0.8, -0);
					ctx.closePath();
					ctx.fill();
					ctx.restore()
				})

				particleSystem.eachNode(function(node, pt) {
					// node: {mass:#, p:{x,y}, name:"", data:{}}
					// pt: {x:#, y:#} node position in screen coords

					// draw a rectangle centered at pt
					
					if (node.data.selected) {
						ctx.fillStyle = "orange";
					} else if (highlighted && node.data.branches.length == 1 && node.data.branches[0] == highlighted) {
						ctx.fillStyle = "green";
					} else {
						ctx.fillStyle = "black"
					}
					
					//ctx.fillRect(pt.x-w/2, pt.y-w/2, w,w)
					ctx.beginPath(); 
					ctx.arc(pt.x, pt.y, Math.pow(node.mass,2) * w, 0, Math.PI*2, true);  
					ctx.closePath();
					ctx.fill();
					ctx.font = "12px Arial";
					if (heads[node.name]) {
						ctx.fillText(heads[node.name].join(", "), pt.x + w + 5 , pt.y + 4);
					}
				})
			},

			initMouseHandling : function() {
				// no-nonsense drag and drop (thanks springy.js)
				var dragged = null;

				// set up a handler object that will initially listen for
				// mousedowns then
				// for moves and mouseups while dragging
				var handler = {
					clicked : function(e) {
						var pos = _$(canvas).offset();
						_mouseP = arbor.Point(e.pageX - pos.left, e.pageY - pos.top)
						dragged = particleSystem.nearest(_mouseP);

						if (dragged && dragged.node !== null) {
							// while we're dragging, don't let physics move the
							// node
							dragged.node.fixed = true
						}

						_$(canvas).bind('mousemove', handler.dragged)
						_$(window).bind('mouseup', handler.dropped)

						return false
					},
					dragged : function(e) {
						var pos = _$(canvas).offset();
						var s = arbor.Point(e.pageX - pos.left, e.pageY - pos.top)

						if (dragged && dragged.node !== null) {
							var p = particleSystem.fromScreen(s)
							dragged.node.p = p
						}

						return false
					},

					dropped : function(e) {
						if (dragged === null || dragged.node === undefined)
							return

						if (dragged.node !== null)
							dragged.node.fixed = false
						dragged.node.tempMass = 1000
						dragged = null
						_$(canvas).unbind('mousemove', handler.dragged)
						_$(window).unbind('mouseup', handler.dropped)
						_mouseP = null
						return false
					}
				}

				// start listening
				_$(canvas).mousedown(handler.clicked);
				

			},
			
			initDoubleClickHandling : function() {
				var selected = null;
				
				var handler = {
					dblclick : function(e) {
						var pos = _$(canvas).offset();
						_mouseP = arbor.Point(e.pageX - pos.left, e.pageY - pos.top);
						if (selected !== undefined && selected !== null) {
							selected.node.data.selected = false;
						}
						selected = particleSystem.nearest(_mouseP);
						
						selectNode(selected.node);
						
						return false;
					},
					
				}
				
				_$(canvas).dblclick(handler.dblclick);
			}

		}
		return that
	}
	
	var selectNode = function(node) {
		node.data.selected = true;
		
		_$('#commitId').text(node.name);
		_$('#author').text(node.data.author);
		_$('#message').text(node.data.message);
		_$('#timestamp').text(new Date(node.data.timestamp).toLocaleString());
		_$('#branches').text(node.data.branches.join(", "));
		
		_$.ajax({
			type: 'GET',
			url: '/stash/rest/api/1.0/projects/' + project + '/repos/' + slug + '/changes?until=' + node.name,
			contentType: "application/json; charset=utf-8",
	        dataType: "json",
	        success: function (data, status, jqXHR) {
	        	 _$('#filetable tr').remove()
	             _$.each(data.values, function() {
	            	 _$('#filetable').append("<tr><td><a href='/stash" + this.link.url + "'>" + this.path['toString'] + "</a></td></tr>")
	             })
	        },

	        error: function (jqXHR, status) {
	             console.log(status);
	        }
		});
		
		_$("#details").show();
	}
	
	var findJunction = function(branch1, branch2) {
		var startNode = getHeadNode(branch1);
		return findFirstBranch(branch2, startNode);
	}
	
	var findFirstBranch = function(branch, startNode) {
		var q = [];
		var visited = [];
		var junctions = [];
		
		console.log('startNode: ' + startNode);
		q.push(startNode);
		
		while (q.length > 0) {
			var node = q[0];
			q.shift();
			visited.push(node);
			console.log('q: ' + node);
			if (inArray(branch, node.data.branches)) {
				junctions.push(node);
			} else {
				for (var i in node.data.parentIds) {
					var parentId = node.data.parentIds[i]
					console.log('parentId: ' + parentId)
					var parentNode = sys.getNode(parentId);
					console.log('parentNode: ' + parentNode)
					if (! inArray(parentNode, visited)) {
						q.push(parentNode);
					}
				}
			}
		}
		
		console.log("junctions: " + junctions.join(", "))
		return findYoungest(junctions);
	}
	
	var findYoungest = function(junctions) {
		var parents = [];
		var visited = [];
	
		var q = junctions.slice(0);
		
		while (q.length > 0) {
			var node = q[0];
			q.shift();
			visited.push[node];
			for (var i in node.data.parentIds) {
				var parentId = node.data.parentIds[i]
				var parentNode = sys.getNode(parentId);
				
				if (! inArray(parentNode, visited)) {
					q.push(parentNode);
					if (inArray(parentNode, junctions)) {
						parents.push(parentNode)
					}
				}
			}
		}
		
		var youngest = _$.grep(junctions, function (item) {
		    return ! inArray(item, parents);
		});
		
		return youngest[0];
	}
	
	var getHeadNode = function(branchName) {
		for (var id in heads) {
			if (inArray(branchName, heads[id])) {
				var node = sys.getNode(id);
				console.log('Node: ' + node.name + ' ' + node.data.branches.join(" "));
				return node;
			}
		}
		return undefined;
	}
	
	var inArray = function(value, array) {
		return ! (_$.inArray(value, array) < 0);
	}
	
	var addNodes = function(branch, values) {
		_$.each(values, function() {
			var node = sys.getNode(this.id);
			if (! node) {
				var parentIds = [];
				_$.each(this.parents, function() {
					parentIds.push(this.id)
				});
				
				node = sys.addNode(this.id, { 
					mass: 1.0, 
					selected: false,
					displayId: this.displayId,
					author: this.author.name,
					message:  this.message,
					timestamp: this.authorTimestamp,
					parentIds: parentIds,
					branches: []
					});
				
				console.log(node.name + ": " + node.data.parentIds.join(", "))
			}
			if (! inArray(branch, node.data.branches)) node.data.branches.push(branch);
			console.log(node.name + ": " + node.data.branches.join(", "))
		});
	}
	
	var addEdges = function(values) {
		_$.each(values, function() {
			var thisNode = sys.getNode(this.id);
			_$.each(this.parents, function() {
				var parentNode = sys.getNode(this.id);
				sys.addEdge(parentNode, thisNode);
			});
		});
	}
	
	var highlightBranch =  function(branch) {
		highlighted = branch;
		sys.renderer.redraw();
	}
	
	var updateBranches = function() {
		console.log("updating branches");
		selectedBranches = [];
		_$.each(_$("#branchesToShow > option:selected"), function() {
			console.log("Adding " + this.text);
			selectedBranches.push(this.text);
		});
		
		var newlimit = _$("#limit > option:selected")[0].text;
		var limitChanged = false;
		if (newlimit != limit) {
			limit = newlimit;
			limitChanged = true;
		}
		
		sys.prune(function(node, from, to) {
			for (var i in node.data.branches) {
				if (inArray(node.data.branches[i], selectedBranches) && ! limitChanged) {
					return false;
				}
			}
			return true;
		});
		
		_$("#branchButtons > input").remove();
		highlighted = undefined;
		graphBranches();
	}
	
	var graphHistory = function(_slug, _project) {
		project = _project
		slug = _slug
		
		sys = arbor.ParticleSystem(200, 800, 0.7) 
		sys.parameters({
			gravity : true
		}) 
		sys.renderer = Renderer("#viewport"); 
		
		graphBranches();
	}
		
	var graphBranches = function() {
		_$.ajax({
			type: 'GET',
				url: '/stash/rest/api/1.0/projects/' + project + '/repos/' + slug + '/branches',
				contentType: "application/json; charset=utf-8",
		        dataType: "json",
		        success: function (data, status, jqXHR) {
		        	_$("#branchesToShow > option").remove()
		        	_$.each(data.values, function() {
		    			if (heads[this.latestChangeset]) {
		    				if (! inArray(this.displayId, heads[this.latestChangeset])) 
		    					heads[this.latestChangeset].push(this.displayId);
		    			} else {
		    				heads[this.latestChangeset] = [ this.displayId ];
		    			}
		    			
		    			var selected = '';
		    			if (inArray(this.displayId, selectedBranches)) {
		    				_$("#branchButtons").append("<input type='button' class='aui-button' value='" + this.displayId + "' onclick='highlightBranch(\"" + this.displayId + "\")' />");
		    				graphBranch(this.id, this.displayId);
		    				selected = 'selected';
		    			}
		    			
		    			_$("#branchesToShow").append("<option value='" + this.id + "'" + selected + ">" + this.displayId + "</option>");
		    		});
		        },
	
		        error: function (jqXHR, status) {
		             console.log(status);
		        }
		})
	}
	
	var graphBranch = function(id, displayId) {
		var limitParam = '';
		if (limit != "all") {
			limitParam = '&limit=' + limit;
		}
		
		_$.ajax({
			type: 'GET',
			url: '/stash/rest/api/1.0/projects/' + project + '/repos/' + slug + '/commits?until=' + id + limitParam,
			contentType: "application/json; charset=utf-8",
	        dataType: "json",
	        success: function (data, status, jqXHR) {
	             addNodes(displayId, data.values);
	             addEdges(data.values);
	        },

	        error: function (jqXHR, status) {
	             console.log(status);
	        }
		});
	}
	
